Compilazione:

``mvn clean package``

I ``.jar`` compilati vanno inclusi tra le dipendenze

1. Da buildpath
2. Nel ``pom.xml`` con opportuno tag ``<dependency/>`` (VD sotto)

```xml
<dependency>
    <artifactId>..</artifactId>
    <groupId>..</groupId>
    <scope>system</scope>
    <systemPath>file://[percorso_jar]/[nome_jar]</systemPath>
</dependency>
```