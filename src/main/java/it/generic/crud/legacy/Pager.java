package it.generic.crud.legacy;

import java.io.Serializable;

public class Pager implements Serializable {

	private static final long serialVersionUID = 4639556997492193469L;

	public static final Integer MAX_RESULTS = 10000;

	public static final Long DEFAULT_TOTAL_RECORDS = 10000L;

	private Integer currentPage = 0;// pagina corrente

	private Integer rowsPerPage = 0;

	private Long totalRecords = (long) 0; // / numero totale di record

	private Integer initialRecord = 0;

	private Integer totalPages = 0; // numero di pagine

	public Pager() {
		this(1, MAX_RESULTS, DEFAULT_TOTAL_RECORDS);
	}

	public Pager(Integer currentPage, Integer rowsPerPage, Long totalRecords) {
		setCurrentPage(currentPage);
		setRowsPerPage(rowsPerPage);
		setInitialRecord(((currentPage - 1) * rowsPerPage));
		setTotalPages((int) Math.ceil((double) totalRecords / (double) rowsPerPage));
		setTotalRecords(totalRecords);
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(Integer rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getInitialRecord() {
		return initialRecord;
	}

	public void setInitialRecord(Integer initialRecord) {
		this.initialRecord = initialRecord;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentPage == null) ? 0 : currentPage.hashCode());
		result = prime * result + ((initialRecord == null) ? 0 : initialRecord.hashCode());
		result = prime * result + ((rowsPerPage == null) ? 0 : rowsPerPage.hashCode());
		result = prime * result + ((totalPages == null) ? 0 : totalPages.hashCode());
		result = prime * result + ((totalRecords == null) ? 0 : totalRecords.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pager other = (Pager) obj;
		if (currentPage == null) {
			if (other.currentPage != null)
				return false;
		} else if (!currentPage.equals(other.currentPage))
			return false;
		if (initialRecord == null) {
			if (other.initialRecord != null)
				return false;
		} else if (!initialRecord.equals(other.initialRecord))
			return false;
		if (rowsPerPage == null) {
			if (other.rowsPerPage != null)
				return false;
		} else if (!rowsPerPage.equals(other.rowsPerPage))
			return false;
		if (totalPages == null) {
			if (other.totalPages != null)
				return false;
		} else if (!totalPages.equals(other.totalPages))
			return false;
		if (totalRecords == null) {
			if (other.totalRecords != null)
				return false;
		} else if (!totalRecords.equals(other.totalRecords))
			return false;
		return true;
	}

}
