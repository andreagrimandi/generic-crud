package it.generic.crud.legacy;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.sql.JoinType;

public class Condition implements Serializable {

	private static final long serialVersionUID = -2206588992682207381L;

	private Criteria criteria;

	private Map<String, String> aliases = new HashMap<String, String>();

	public Condition(Criteria criteria) {
		this.criteria = criteria;
	}

	public Criteria getCriteria() {
		return criteria;
	}

	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}

	public Map<String, String> getAliases() {
		return aliases;
	}

	public void setAliases(Map<String, String> aliases) {
		this.aliases = aliases;
	}

	public String createAlias(String associationPath) {
		return createAlias(associationPath, false);
	}

	public String createAlias(String associationPath, boolean leftOuterJoin) {
		String alias = aliases.get(associationPath);
		if (alias == null) {
			alias = associationPath + "_alias";
			aliases.put(associationPath, alias);
			criteria.createAlias(associationPath, alias,
					(leftOuterJoin ? JoinType.LEFT_OUTER_JOIN : JoinType.INNER_JOIN));
		}
		return alias;
	}

	public String splitNameField(String field) {
		return splitNameField(field, false);
	}

	public String splitNameField(String field, boolean leftOuterJoin) {
		String fieldName = null;
		String[] arrayOrderField = field.split("\\.", 2);
		if (arrayOrderField.length == 1)
			fieldName = arrayOrderField[0];
		else {
			fieldName = arrayOrderField[1];
			fieldName = createAlias(arrayOrderField[0], leftOuterJoin) + "." + arrayOrderField[1];
		}
		return fieldName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aliases == null) ? 0 : aliases.hashCode());
		result = prime * result + ((criteria == null) ? 0 : criteria.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Condition other = (Condition) obj;
		if (aliases == null) {
			if (other.aliases != null)
				return false;
		} else if (!aliases.equals(other.aliases))
			return false;
		if (criteria == null) {
			if (other.criteria != null)
				return false;
		} else if (!criteria.equals(other.criteria))
			return false;
		return true;
	}

}
