package it.generic.crud.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class JqGridData {

	private Integer page = 0;// pagina corrente

	private Integer total = 0; // numero totale di pagine

	private Long records = (long) 0; // / numero totale di record

	private List<?> rows; // / dati

	public JqGridData() {

	}

	public JqGridData(Integer page, Long records, Integer total) {
		setPage(page);
		setRecords(records);
		setTotal(total);
	}

	public JqGridData(Pager pager) {
		if (pager != null) {
			setPage(pager.getCurrentPage());
			setRecords(pager.getTotalRecords());

			if (pager.getCurrentPage() == 0) {
				setTotal(0);
			} else {
				setTotal(pager.getTotalPages());
			}

		}
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Long getRecords() {
		return records;
	}

	public void setRecords(Long records) {
		this.records = records;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public String toJson() {
		return new JSONSerializer().exclude("*.class").include("rows").serialize(this);
	}

	public String toJsonWith(String list) {
		return new JSONSerializer().exclude("*.class").include("rows").include("rows." + list).serialize(this);
	}

	public String toJsonWithLists(String list, String list2) {
		return new JSONSerializer().exclude("*.class").include("rows").include("rows." + list).include("rows." + list2)
				.serialize(this);
	}

	public String toJson(String[] fields) {
		return new JSONSerializer().include(fields).exclude("*.class").serialize(this);
	}

	public static JqGridData fromJsonToJqGridData(String json) {
		return new JSONDeserializer<JqGridData>().use(null, JqGridData.class).deserialize(json);
	}

	public static String toJsonArray(Collection<JqGridData> collection) {
		return new JSONSerializer().exclude("*.class").serialize(collection);
	}

	public static String toJsonArray(Collection<JqGridData> collection, String[] fields) {
		return new JSONSerializer().include(fields).exclude("*.class").serialize(collection);
	}

	public static Collection<JqGridData> fromJsonArrayToJqGridData(String json) {
		return new JSONDeserializer<List<JqGridData>>().use(null, ArrayList.class).use("values", JqGridData.class)
				.deserialize(json);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((page == null) ? 0 : page.hashCode());
		result = prime * result + ((records == null) ? 0 : records.hashCode());
		result = prime * result + ((rows == null) ? 0 : rows.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JqGridData other = (JqGridData) obj;
		if (page == null) {
			if (other.page != null)
				return false;
		} else if (!page.equals(other.page))
			return false;
		if (records == null) {
			if (other.records != null)
				return false;
		} else if (!records.equals(other.records))
			return false;
		if (rows == null) {
			if (other.rows != null)
				return false;
		} else if (!rows.equals(other.rows))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		return true;
	}

}
