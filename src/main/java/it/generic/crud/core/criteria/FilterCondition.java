package it.generic.crud.core.criteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Crea condizioni di filtro associate a uno o più campi designati in
 * <b>fieldNames</b>.
 * 
 * Se dentro a <b>fieldNames</b> vi è un singolo valore e il suddetto
 * corrisponde al nome di un campo del bean per il quale si vuole effettuare la
 * query, se <b>getCriterionFilterCondition()</b> non ritorna un valore null, il
 * filtro di default su quel campo verrà overridato dalla condizione ritornata
 * dalla funzione stessa.
 * 
 * Nel caso <b>fieldNames</b> fosse nummo o <b>size() == 0</b> (non dichiarato
 * nel costruttore) significa che la condizione di filtro dev'essere applicata
 * sempre ad ogni ricerca indifferentemente dai filtri impostati in interfaccia
 * 
 * @author Andrea_Grimandi
 *
 */
public abstract class FilterCondition {

	private List<String> fieldNames;

	/**
	 * 
	 * @param fieldName
	 */
	public FilterCondition() {
		super();
		this.fieldNames = new ArrayList<>();
	}

	/**
	 * 
	 * @param fieldName
	 */
	public FilterCondition(String... fieldName) {
		super();
		this.fieldNames = Arrays.asList(fieldName);
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getFieldNames() {
		return fieldNames;
	}

	/**
	 * 
	 * @param fieldName
	 */
	public void setFieldNames(String... fieldName) {
		this.fieldNames = Arrays.asList(fieldName);
	}

	/**
	 * definisce la logica di filtro che dev'essere applicata sul campo definito in
	 * <b>fieldNames</b>
	 * 
	 * @param filters
	 *            json di filtri provenienti dal frontend
	 * @return
	 */
	public Criterion getFilterCondition(ObjectNode filters) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FilterCondition [fieldNames=" + fieldNames + "]";
	}

}
