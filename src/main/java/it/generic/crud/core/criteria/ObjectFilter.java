package it.generic.crud.core.criteria;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * I filtri vengono applicati in maniera ordinata, in questo modo si possono
 * ottimizzare le query di ricerca.
 * 
 * Filtri esplicitati sullo stesso campo avranno una logica di esclusione.
 * 
 * Es:
 * 
 * .addCriterionFilterCondition(new CriterionFilterCondition("insertDateFrom",
 * "insertDateTo"){...});
 * 
 * .addCriterionFilterCondition(new
 * CriterionFilterCondition("insertDateTo"){...});
 * 
 * il secondo filtro non viene applicato, in quanto il campo <b>insertDateTo</b>
 * è già stato esplicitato nel primo. Invertendo l'ordine si avrà l'effetto
 * opposto.
 * 
 * @author Andrea_Grimandi
 *
 */
public class ObjectFilter {

	/**
	 * 
	 */
	private List<FilterCondition> filterConditions;

	/**
	 * 
	 */
	private List<String> usedFilters;

	/**
	 * 
	 */
	private List<FilterCondition> remainingFilters;

	/**
	 * 
	 */
	private ObjectNode htmlJsonFilter;

	/**
	 * 
	 */
	public ObjectFilter() {
		this.filterConditions = new ArrayList<>();
		this.usedFilters = new ArrayList<>();
		this.remainingFilters = new ArrayList<>();
	}

	/**
	 * 
	 * @return
	 */
	public ObjectNode getHtmlJsonFilter() {
		return htmlJsonFilter;
	}

	/**
	 * 
	 * @param filters
	 */
	public void setHtmlJsonFilter(ObjectNode filters) {
		this.htmlJsonFilter = filters;
	}

	/**
	 * 
	 * @return
	 */
	public List<FilterCondition> getCriterionFilterConditions() {
		return filterConditions;
	}

	/**
	 * 
	 * @param criterionFilterConditions
	 */
	public void setCriterionFilterConditions(List<FilterCondition> criterionFilterConditions) {
		this.filterConditions = criterionFilterConditions;
	}

	/**
	 * 
	 * @param criterionFilterCondition
	 * @return
	 */
	public ObjectFilter addFilterCondition(FilterCondition criterionFilterCondition) {
		this.filterConditions.add(criterionFilterCondition);
		return this;
	}

	/**
	 * Nel caso venga trovato un filtro associato ad un campo del bean per il quale
	 * si sta effettuando la query viene ritornato quel determinato filtro
	 * 
	 * @param fieldName
	 * @return
	 */
	public FilterCondition getFilterByFieldName(String fieldName) {
		for (FilterCondition filterCondition : this.filterConditions) {
			if (filterCondition.getFieldNames().contains(fieldName)) {
				this.usedFilters.addAll(filterCondition.getFieldNames());
				return filterCondition;
			}
			return null;
		}
		return null;
	}

	/**
	 * Ritorna tutte le condizioni di filtro che non sono state associate a nessun
	 * campo del bean per il quale si sta effettuando la query.
	 * 
	 * @return
	 */
	public List<FilterCondition> remainingFilters() {
		for (FilterCondition criterionPair : this.filterConditions) {
			if (!this.usedFilters.containsAll(criterionPair.getFieldNames())
					|| criterionPair.getFieldNames().size() == 0) {
				this.remainingFilters.add(criterionPair);
			}
		}
		return this.remainingFilters;
	}

}
