package it.generic.crud.core.criteria;

/**
 * 
 * @author Andrea_Grimandi
 *
 */
public interface ConditionFilter {

	/**
	 * 
	 */
	public abstract ObjectFilter getFilter();

}
