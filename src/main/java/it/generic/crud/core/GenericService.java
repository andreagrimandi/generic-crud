package it.generic.crud.core;

import java.io.IOException;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.generic.crud.core.criteria.ConditionFilter;
import it.generic.crud.core.criteria.ObjectFilter;
import it.generic.crud.core.filter.SimpleCustomFilter;
import it.generic.crud.legacy.JqGridData;
import it.generic.crud.legacy.Pager;

/**
 * Wrapper per le operazioni più comuni attuabili sui modelli
 * 
 * @author Andrea_Grimandi
 *
 * @param <E>
 */
public class GenericService<E> {

	/**
	 * 
	 */
	private GenericDao<E> genericDao;

	/**
	 * 
	 */
	private Class<?> E;

	/**
	 * 
	 * @param e
	 */
	public GenericService<E> setEntity(Class<?> e) {
		this.E = e;
		return this;
	}

	/**
	 * 
	 * @param genericDao
	 */
	public GenericService<E> setDao(GenericDao<E> genericDao) {
		this.genericDao = genericDao;
		this.genericDao.init();
		return this;
	}

	/**
	 * da overridare
	 */
	public GenericService<E> init() {
		return this;
	}

	/**
	 * 
	 * @return
	 */
	@Transactional
	public List<E> getAll() {
		this.init();
		return genericDao.getAll();
	}

	/**
	 * 
	 * @param json
	 * @throws Exception
	 */
	@Transactional
	public void create(E obj) throws Exception {
		this.init();
		genericDao.create(obj);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public E read(Integer id) {
		this.init();
		return genericDao.read(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public E find(Integer id) {
		this.init();
		return genericDao.find(id);
	}

	/**
	 * 
	 * @param id
	 * @param json
	 * @throws Exception
	 */
	@Transactional
	public void update(E obj) throws Exception {
		this.init();
		genericDao.update(obj);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public Integer delete(Integer id) {
		this.init();
		return genericDao.delete(id);
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	@Transactional
	public void delete(E obj) {
		this.init();
		genericDao.delete(obj);
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filter(E objFilter) throws Exception {
		this.init();
		return genericDao.filter(objFilter);
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filter(SimpleCustomFilter objFilter) throws Exception {
		this.init();
		return genericDao.filter(objFilter);
	}

	// ****************************************************************************************************************

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Long count(E objFilter) throws Exception {
		this.init();
		return genericDao.count(objFilter);
	}

	/**
	 * 
	 * @param objFilter
	 * @param criterionFilter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Long count(E objFilter, ObjectFilter criterionFilter) throws Exception {
		this.init();
		return genericDao.count(objFilter, criterionFilter);
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Long count(SimpleCustomFilter objFilter) throws Exception {
		this.init();
		return genericDao.count(objFilter);
	}

	// ****************************************************************************************************************

	/*
	 * Queste funzioni restituiscono un json che rappresenta la struttura dati
	 * adatta ad essere itnerpretata da una jqgrid.
	 * 
	 * NB: Riutilizzati oggetti e funzioni già esistenti centralizzandoli in un
	 * unico punto. Non c'è più bisogno di usare una entità filtro ma basta
	 * specificare i criteri di filtro valorizzando gli attributi dell'oggetto sul
	 * quale si vuole effettuare la ricerca
	 * 
	 */

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(String filter, Integer currentPage, Integer maxRowsToShow, String sidx,
			String ordering) throws Exception {
		this.init();
		E entityFilter = !isvoid(filter) ? this.fromJsonToEntity(filter) : null;
		return this.filterJqGridJson(entityFilter, currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(E filter, Integer currentPage, Integer maxRowsToShow, String sidx, String ordering)
			throws Exception {
		this.init();
		Pager pager = this.buildPager(currentPage, maxRowsToShow, filter);
		List<E> list = genericDao.filterJqGrid(filter, sidx, ordering, pager);
		return this.getJqGrid(pager, list).toJson();
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(String filter, ConditionFilter criterionFilter, Integer currentPage,
			Integer maxRowsToShow, String sidx, String ordering) throws Exception {
		this.init();
		return this.filterJqGridJson(filter, criterionFilter.getFilter(), currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(String filter, ObjectFilter criterionFilter, Integer currentPage,
			Integer maxRowsToShow, String sidx, String ordering) throws Exception {
		this.init();
		E entityFilter = !isvoid(filter) ? this.fromJsonToEntity(filter) : null;
		criterionFilter
				.setHtmlJsonFilter(new ObjectMapper().readValue(!isvoid(filter) ? filter : "{}", ObjectNode.class));
		return this.filterJqGridJson(entityFilter, criterionFilter, currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(E filter, ConditionFilter criterionFilter, Integer currentPage,
			Integer maxRowsToShow, String sidx, String ordering) throws Exception {
		this.init();
		return this.filterJqGridJson(filter, criterionFilter.getFilter(), currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(E filter, ObjectFilter criterionFilter, Integer currentPage, Integer maxRowsToShow,
			String sidx, String ordering) throws Exception {
		this.init();
		Pager pager = this.buildPager(currentPage, maxRowsToShow, filter, criterionFilter);
		List<E> list = genericDao.filterJqGrid(filter, criterionFilter, sidx, ordering, pager);
		return this.getJqGrid(pager, list).toJson();
	}

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String filterJqGridJson(SimpleCustomFilter filter, Integer currentPage, Integer maxRowsToShow, String sidx,
			String ordering) throws Exception {
		this.init();
		Pager pager = this.buildPager(currentPage, maxRowsToShow, filter);
		List<E> list = genericDao.filterJqGrid(filter, sidx, ordering, pager);
		return this.getJqGrid(pager, list).toJson();
	}

	// ****************************************************************************************************************

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(String filter, Integer currentPage, Integer maxRowsToShow, String sidx, String ordering)
			throws Exception {
		this.init();
		E entityFilter = !isvoid(filter) ? this.fromJsonToEntity(filter) : null;
		return this.filterList(entityFilter, currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(E filter, Integer currentPage, Integer maxRowsToShow, String sidx, String ordering)
			throws Exception {
		this.init();
		Pager pager = this.buildPager(currentPage, maxRowsToShow, filter);
		return genericDao.filterJqGrid(filter, sidx, ordering, pager);
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(String filter, ConditionFilter criterionFilter, Integer currentPage,
			Integer maxRowsToShow, String sidx, String ordering) throws Exception {
		this.init();
		return this.filterList(filter, criterionFilter.getFilter(), currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(E filter, ConditionFilter criterionFilter, Integer currentPage, Integer maxRowsToShow,
			String sidx, String ordering) throws Exception {
		this.init();
		return this.filterList(filter, criterionFilter.getFilter(), currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(String filter, ObjectFilter criterionFilter, Integer currentPage, Integer maxRowsToShow,
			String sidx, String ordering) throws Exception {
		this.init();
		E entityFilter = !isvoid(filter) ? this.fromJsonToEntity(filter) : null;
		criterionFilter
				.setHtmlJsonFilter(new ObjectMapper().readValue(!isvoid(filter) ? filter : "{}", ObjectNode.class));
		return this.filterList(entityFilter, criterionFilter, currentPage, maxRowsToShow, sidx, ordering);
	}

	/**
	 * 
	 * @param filter
	 * @param criterionFilter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(E filter, ObjectFilter criterionFilter, Integer currentPage, Integer maxRowsToShow,
			String sidx, String ordering) throws Exception {
		this.init();
		Pager pager = this.buildPager(currentPage, maxRowsToShow, filter);
		return genericDao.filterJqGrid(filter, criterionFilter, sidx, ordering, pager);
	}

	/**
	 * 
	 * @param filter
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param sidx
	 * @param ordering
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<E> filterList(SimpleCustomFilter filter, Integer currentPage, Integer maxRowsToShow, String sidx,
			String ordering) throws Exception {
		this.init();
		Pager pager = this.buildPager(currentPage, maxRowsToShow, filter);
		return genericDao.filterJqGrid(filter, sidx, ordering, pager);
	}

	// ****************************************************************************************************************
	// UTILITIES

	/**
	 * 
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	private Pager buildPager(Integer currentPage, Integer maxRowsToShow, E filter) throws Exception {
		if ((currentPage != null) && (maxRowsToShow != null)) {
			return new Pager(currentPage, maxRowsToShow, this.count(filter));
		}

		return null;
	}

	/**
	 * 
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param filter
	 * @param criterionFilter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	private Pager buildPager(Integer currentPage, Integer maxRowsToShow, E filter, ObjectFilter criterionFilter)
			throws Exception {
		if ((currentPage != null) && (maxRowsToShow != null)) {
			return new Pager(currentPage, maxRowsToShow, this.count(filter, criterionFilter));
		}

		return null;
	}

	/**
	 * 
	 * @param currentPage
	 * @param maxRowsToShow
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	@Transactional
	private Pager buildPager(Integer currentPage, Integer maxRowsToShow, SimpleCustomFilter filter) throws Exception {
		if ((currentPage != null) && (maxRowsToShow != null)) {
			return new Pager(currentPage, maxRowsToShow, this.count(filter));
		}

		return null;
	}

	/**
	 * 
	 * @param pager
	 * @param list
	 * @return
	 */
	@Transactional
	private JqGridData getJqGrid(Pager pager, List<E> list) {
		JqGridData grid = new JqGridData(pager);
		grid.setRows(list);
		return grid;
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	@Transactional
	private E fromJsonToEntity(String json) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JavaType type = mapper.getTypeFactory().constructType(this.E);
		try {
			return mapper.readValue(json, type);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	// ****************************************************************************************************************

	/**
	 * 
	 * @param pArg
	 * @return
	 */
	private static boolean isvoid(String pArg) {
		return (pArg == null || "".equals(pArg.trim()));
	}

}
