package it.generic.crud.core;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.NullPrecedence;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import it.generic.crud.core.criteria.ObjectFilter;
import it.generic.crud.core.filter.SimpleCustomFilter;
import it.generic.crud.core.filter.GenericFilter;
import it.generic.crud.legacy.Condition;
import it.generic.crud.legacy.Pager;

/**
 * Wrapper per le operazioni pi� comuni attuabili sui modelli
 * 
 * @author Andrea_Grimandi
 *
 * @param <E>
 */
public class GenericDao<E> {

	/**
	 * 
	 */
	private SessionFactory sessionFactory;

	/**
	 * 
	 */
	private Class<?> E;

	/**
	 * 
	 * @param e
	 */
	public GenericDao<E> setEntity(Class<?> e) {
		this.E = e;
		return this;
	}

	/**
	 * Imposta la SessionFactory per effettuare le query sul database corretto
	 * 
	 * @param sessionFactory
	 */
	public GenericDao<E> setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		return this;
	}

	/**
	 * da overridare
	 */
	public void init() {

	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<E> getAll() {
		this.init();
		return sessionFactory.getCurrentSession().createCriteria(this.E).list();
	}

	/**
	 * 
	 * @param obj
	 */
	public void create(E obj) {
		this.init();
		sessionFactory.getCurrentSession().save(obj);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public E read(Integer id) {
		this.init();
		return (E) sessionFactory.getCurrentSession().createCriteria(this.E).add(Restrictions.eq("id", id))
				.uniqueResult();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public E find(Integer id) {
		this.init();
		return (E) sessionFactory.getCurrentSession().createCriteria(this.E).add(Restrictions.eq("id", id))
				.uniqueResult();
	}

	/**
	 * 
	 * @param id
	 * @param obj
	 */
	public void update(E obj) {
		this.init();
		sessionFactory.getCurrentSession().update(obj);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Integer delete(Integer id) {
		this.init();
		return sessionFactory.getCurrentSession().createQuery("delete " + this.E.getSimpleName() + " where id = :id")
				.setInteger("id", id).executeUpdate();
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public void delete(E obj) {
		this.init();
		sessionFactory.getCurrentSession().delete(obj);
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<E> filter(E objFilter) throws Exception {
		this.init();
		return (List<E>) new GenericFilter<E>(sessionFactory.getCurrentSession().createCriteria(this.E))
				.getCriteria(objFilter).list();
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<E> filter(SimpleCustomFilter objFilter) throws Exception {
		this.init();
		return (List<E>) new GenericFilter<E>(sessionFactory.getCurrentSession().createCriteria(this.E))
				.getCriteria(objFilter).list();
	}

	// ****************************************************************************************************************

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 * @throws HibernateException
	 */
	public Long count(E objFilter) throws Exception {
		this.init();
		Object uniqueResult = new GenericFilter<E>(sessionFactory.getCurrentSession().createCriteria(this.E))
				.getCriteria(objFilter).setProjection(Projections.rowCount()).uniqueResult();
		return uniqueResult == null ? 0 : (Long) uniqueResult;
	}

	/**
	 * 
	 * @param objFilter
	 * @param criterionFilter
	 * @return
	 * @throws Exception
	 */
	public Long count(E objFilter, ObjectFilter criterionFilter) throws Exception {
		this.init();
		Object uniqueResult = new GenericFilter<E>(sessionFactory.getCurrentSession().createCriteria(this.E))
				.getCriteria(objFilter, criterionFilter).setProjection(Projections.rowCount()).uniqueResult();
		return uniqueResult == null ? 0 : (Long) uniqueResult;
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 * @throws HibernateException
	 */
	public Long count(SimpleCustomFilter objFilter) throws Exception {
		this.init();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(this.E);
		Object uniqueResult = new GenericFilter<E>(criteria).getCriteria(objFilter.getFilter(criteria))
				.setProjection(Projections.rowCount()).uniqueResult();
		return uniqueResult == null ? 0 : (Long) uniqueResult;
	}

	// ****************************************************************************************************************

	/*
	 * Queste funzioni restituiscono una lista di Entità di un determinato modello
	 * paginato.
	 * 
	 * NB: BaseList creata in modo generico riutilizzando le classi già presenti.
	 * Filter utilizzato come classe anonima in quanto non più necessaria entità di
	 * appoggio "[NomeEntitaModello]Filter" ma vengono utilizzati direttamente i
	 * valori delle proprietà presenti nell'oggetto "filters" che rispecchia il
	 * modello sul quale si sta effettuando il filtro.
	 */

	/**
	 * 
	 * @param filters
	 * @param sidx
	 * @param ordering
	 * @param pager
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<E> filterJqGrid(E filters, String sidx, String ordering, Pager pager) {
		this.init();
		try {
			Criteria criteria = new GenericFilter<E>(sessionFactory.getCurrentSession().createCriteria(this.E))
					.getCriteria(filters);
			criteria = this.applyOrderPager(criteria, pager, sidx, ordering);
			return criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	/**
	 * 
	 * @param filters
	 * @param criterionFilter
	 * @param sidx
	 * @param ordering
	 * @param pager
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<E> filterJqGrid(E filters, ObjectFilter criterionFilter, String sidx, String ordering, Pager pager) {
		this.init();
		try {
			Criteria criteria = new GenericFilter<E>(sessionFactory.getCurrentSession().createCriteria(this.E))
					.getCriteria(filters, criterionFilter);
			criteria = this.applyOrderPager(criteria, pager, sidx, ordering);
			return criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	/**
	 * 
	 * @param filters
	 * @param sidx
	 * @param ordering
	 * @param pager
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<E> filterJqGrid(SimpleCustomFilter filters, String sidx, String ordering, Pager pager) {
		this.init();
		try {
			Criteria emptyCriteria = sessionFactory.getCurrentSession().createCriteria(this.E);
			Criteria criteria = new GenericFilter<E>(emptyCriteria).getCriteria(filters.getFilter(emptyCriteria));
			criteria = this.applyOrderPager(criteria, pager, sidx, ordering);
			return criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	// ****************************************************************************************************************

	/**
	 * 
	 */
	public static final String ORDER_TYPE_ASC = "ASC";

	/**
	 * 
	 */
	public static final String ORDER_TYPE_DESC = "DESC";

	/**
	 * 
	 * @param criteria
	 * @param pager
	 * @param orderField
	 * @param orderType
	 * @return
	 */
	protected Criteria applyOrderPager(Criteria criteria, Pager pager, String orderField, String orderType) {
		Condition condition = new Condition(criteria);

		if (pager != null) {
			criteria.setMaxResults(pager.getRowsPerPage());
			criteria.setFirstResult(pager.getInitialRecord());
		}

		if (orderField != null) {

			String[] fields = orderField.split(",");

			for (String iter : fields) {

				iter = iter.trim();
				String[] maybeOrder = iter.split(" ");

				if (maybeOrder.length == 1) {

					String field = condition.splitNameField(iter, true);

					if (orderType.equalsIgnoreCase(ORDER_TYPE_ASC)) {
						criteria.addOrder(Order.asc(field).nulls(NullPrecedence.LAST));
					} else if (orderType.equalsIgnoreCase(ORDER_TYPE_DESC)) {
						criteria.addOrder(Order.desc(field).nulls(NullPrecedence.LAST));
					} else {
						throw new RuntimeException("OrderType " + orderType + " Not Valid");
					}

				} else {

					String field = condition.splitNameField(maybeOrder[0], true);

					if (maybeOrder[1].equalsIgnoreCase(ORDER_TYPE_ASC)) {
						criteria.addOrder(Order.asc(field).nulls(NullPrecedence.LAST));
					} else if (maybeOrder[1].equalsIgnoreCase(ORDER_TYPE_DESC)) {
						criteria.addOrder(Order.desc(field).nulls(NullPrecedence.LAST));
					} else {
						throw new RuntimeException("OrderType " + orderType + " Not Valid");
					}

				}

			}

		}

		return criteria;
	}

}
