package it.generic.crud.core.filter;

import java.beans.Introspector;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Transient;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import it.generic.crud.core.criteria.FilterCondition;
import it.generic.crud.core.criteria.ObjectFilter;

/**
 * Generatore di condizioni di where per hibernate
 * 
 * @author Andrea_Grimandi
 *
 * @param <E>
 */
public class GenericFilter<E> {

	/**
	 * 
	 */
	private Criteria criteria;

	/**
	 * 
	 * @param criteria
	 */
	public GenericFilter(Criteria criteria) {
		super();
		this.criteria = criteria;
	}

	/**
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	public Criteria getCriteria(SimpleCustomFilter objFilter) throws Exception {
		if (objFilter.getFilter(this.criteria) != null) {
			this.criteria.add(objFilter.getFilter(this.criteria));
		}
		return this.criteria;
	}

	/**
	 * Criteri di filtro custom
	 * 
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public Criteria getCriteria(Criterion filter) throws Exception {
		return this.criteria.add(filter);
	}

	/**
	 * Costruisce i criteri di filtro in funzione degli attributi del modello
	 * valorizzati.
	 * 
	 * @param objFilter
	 * @return
	 * @throws Exception
	 */
	public Criteria getCriteria(E objFilter) throws Exception {
		if (objFilter != null) {
			for (Method method : Arrays.asList(objFilter.getClass().getDeclaredMethods())) {
				if (method.getName().contains("get") && method.getAnnotation(Transient.class) == null) {
					Object value = method.invoke(objFilter, (Object[]) null);
					if (value != null) {
						// nome del campo della classe preso a partire dai getter (in quanto siano
						// private e non accessibili tramite reflection)
						String fieldName = Introspector.decapitalize(method.getName().substring(3));
						// se stringa, ilike
						if (method.getReturnType().equals(String.class)) {
							this.criteria.add(Restrictions.ilike(fieldName, "%" + value.toString() + "%"));
						} else {
							this.criteria.add(Restrictions.eq(fieldName, value));
						}
					}
				}
			}
		}
		return this.criteria;
	}

	/**
	 * Costruisce i criteria con i quale effettuare la query con hibernate
	 * 
	 * @param objFilter
	 * @param criterionFilter
	 * @return
	 * @throws Exception
	 */
	public Criteria getCriteria(E objFilter, ObjectFilter criterionFilter) throws Exception {
		if (objFilter != null) {
			for (Method method : Arrays.asList(objFilter.getClass().getDeclaredMethods())) {
				if (method.getName().contains("get") && method.getAnnotation(Transient.class) == null) {
					Object value = method.invoke(objFilter, (Object[]) null);
					if (value != null) {
						// nome del campo della classe preso a partire dai getter (in quanto siano
						// private e non accessibili tramite reflection)
						String fieldName = Introspector.decapitalize(method.getName().substring(3));

						FilterCondition criterionPair = criterionFilter.getFilterByFieldName(fieldName);

						// viene controllato se per quel determinato campo non vi è un filtro custom.
						// Se
						// c'è, viene usato come criterio quello esplicitato qui.
						if (criterionPair != null) {
							Criterion criterion = criterionPair.getFilterCondition(criterionFilter.getHtmlJsonFilter());
							if (criterion != null) {
								this.criteria.add(criterion);
							}
						} else {
							// se stringa, ilike
							if (method.getReturnType().equals(String.class)) {
								this.criteria.add(Restrictions.ilike(fieldName, "%" + value.toString() + "%"));
							} else {
								this.criteria.add(Restrictions.eq(fieldName, value));
							}
						}

					}
				}
			}

			// lista di filtri già usati
			List<String> criterionUsed = new ArrayList<>();

			// vengono aggiunte condizioni custom che non sono associate ai campi della
			// classe
			for (FilterCondition criterionPair : criterionFilter.remainingFilters()) {

				// i filtri in più vengono aggiunti alle condizioni di where solamente se:
				//
				// 1. non fanno riferimento a nessun campo in particolare e di conseguenza sono
				// dei filtri che devono essere sempre presenti;
				//
				// 2. sono dei filtri custom.
				//
				// 3. sono dei filtri che fanno riferimento al bean per il quale si vuole
				// effettuare la query.
				boolean isFilterInGridRequest = false;

				for (String fieldName : criterionPair.getFieldNames()) {
					isFilterInGridRequest = criterionFilter.getHtmlJsonFilter().toString().contains(fieldName);

					if (!isFilterInGridRequest) {
						break;
					}
				}

				// viene ricercato il CriterionFilterCondition se è già stato usato.
				//
				// questo per evitare di duplicare condizioni di where sugli stessi campi
				boolean filterAlreadyUsed = false;

				for (String usedFilter : criterionUsed) {
					for (String incomingFilter : criterionPair.getFieldNames()) {
						if (usedFilter.equals(incomingFilter)) {
							filterAlreadyUsed = true;
							break;
						}
					}

					if (filterAlreadyUsed) {
						break;
					}
				}

				// 1. size() == 0 -> filtro sempre presente
				//
				// 2. isFilterInGridRequest -> è presente tra i filtri e perciò dev'essere
				// applicato
				//
				// 3. alreadyUsedFilter -> filtri su campi che sono già stati associati ad un
				// filtro precedentemente, non vengono applicati
				if (isFilterInGridRequest && !filterAlreadyUsed) {
					Criterion criterion = criterionPair.getFilterCondition(criterionFilter.getHtmlJsonFilter());
					if (criterion != null) {
						criterionUsed.addAll(criterionPair.getFieldNames());
						this.criteria.add(criterion);
					}
				}
			}
		}
		return this.criteria;
	}

}
