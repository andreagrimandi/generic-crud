package it.generic.crud.core.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

/**
 * Utilizzata per filtri custom <b>NON PER JQGRID</b>
 * 
 * @author Andrea_Grimandi
 *
 */
public interface SimpleCustomFilter {

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public Criterion getFilter(Criteria criteria);

}
