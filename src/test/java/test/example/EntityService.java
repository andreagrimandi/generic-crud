package test.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.generic.crud.core.GenericService;

@Service
public class EntityService extends GenericService<Entity> {

	@Autowired
	private EntityDao entityDao;

	@Override
	public GenericService<Entity> init() {
		this.setEntity(Entity.class);
		this.setDao(entityDao);
		return super.init();
	}

}
