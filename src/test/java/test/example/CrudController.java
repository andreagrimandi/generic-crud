package test.example;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.generic.crud.core.criteria.ConditionFilter;
import it.generic.crud.core.criteria.FilterCondition;
import it.generic.crud.core.criteria.ObjectFilter;
import it.generic.crud.core.filter.SimpleCustomFilter;

@Controller
@RequestMapping("/crud")
public class CrudController {

	@Autowired
	private EntityService entityService;

	/**
	 * 
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public void crud() throws Exception {
		Entity filter = new Entity();

		String filterJqGridJson = entityService.init().filterJqGridJson(filter, new CrudCriterionFilter(), 1, 20,
				"some_prop", "desc");

		List<Entity> filter2 = entityService.init().filter(new SimpleCustomFilter() {

			@Override
			public Criterion getFilter(Criteria criteria) {
				return null;
			}

		});

		List<Entity> filter3 = entityService.init().filter(filter);

		entityService.init().delete(filter);
	}

	/**
	 * Endpoint per JQGRID con filtri e pager
	 * 
	 * @param page
	 * @param rows
	 * @param filters
	 * @param sidx
	 * @param sord
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = "filters", headers = "Accept=application/json; charset=UTF-8")
	public String filter(@RequestParam Integer page, @RequestParam Integer rows, @RequestParam String filters,
			@RequestParam String sidx, @RequestParam String sord) throws Exception {
		return entityService.init().filterJqGridJson(filters, page, rows, sidx, sord);
	}

	// ****************************************************************************************************

	/**
	 * 
	 * @author Andrea
	 *
	 */
	class CrudCriterionFilter implements ConditionFilter {

		@Override
		public ObjectFilter getFilter() {

			return new ObjectFilter().addFilterCondition(new FilterCondition() {
				@Override
				public Criterion getFilterCondition(ObjectNode filters) {
					return super.getFilterCondition(filters);
				}
			}).addFilterCondition(new FilterCondition() {
				@Override
				public Criterion getFilterCondition(ObjectNode filters) {
					return super.getFilterCondition(filters);
				}
			});

		}

	}

}
