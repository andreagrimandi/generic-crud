package test.example;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.generic.crud.core.GenericDao;

@Repository
public class EntityDao extends GenericDao<Entity> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void init() {
		super.setSessionFactory(sessionFactory);
		super.setEntity(Entity.class);
	}

}
